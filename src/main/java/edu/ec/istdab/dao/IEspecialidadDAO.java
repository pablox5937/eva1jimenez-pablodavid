package edu.ec.istdab.dao;

import javax.ejb.Local;

import edu.ec.istdab.model.Especialidad;

@Local
public interface IEspecialidadDAO extends ICRUD<Especialidad>{

}
