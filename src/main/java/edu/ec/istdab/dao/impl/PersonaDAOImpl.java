package edu.ec.istdab.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContexts;
import javax.persistence.Query;

import edu.ec.istdab.dao.IPersonaDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Rol;

//@Named
@Stateful
public class PersonaDAOImpl implements IPersonaDAO, Serializable{
	
	@PersistenceContext (unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Persona per) throws Exception {
		em.persist(per);
		return per.getIdPersona();
	}

	@Override
	public Integer modificar(Persona per) throws Exception {
		em.merge(per);
		return 1;
	}

	@Override
	public Integer eliminar(Persona per) throws Exception {
		
		return null;
	}

	@Override
	public List<Persona> listar() throws Exception {
		Query q = em.createQuery("SELECT p FROM Persona p");
		List<Persona> lista = (List<Persona>) q.getResultList();
		return lista;
	}

	@Override
	public Persona listarPorId(Persona per) throws Exception {
		Persona persona = new Persona();
		List<Persona> lista = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Persona p WHERE p.idPersona=?1");
			query.setParameter(1, per.getIdPersona());
			lista = (List<Persona>) query.getResultList();
			if (lista != null && !lista.isEmpty()) {
				persona = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return persona;
	}
	
}
