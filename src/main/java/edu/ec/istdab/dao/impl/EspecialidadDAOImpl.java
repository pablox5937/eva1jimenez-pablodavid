package edu.ec.istdab.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.ec.istdab.dao.IEspecialidadDAO;
import edu.ec.istdab.model.Especialidad;

@Stateful
public class EspecialidadDAOImpl implements IEspecialidadDAO, Serializable{

	@PersistenceContext (unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Especialidad t) throws Exception {
		em.persist(t);
		return t.getIdespecialidad();
	}

	@Override
	public Integer modificar(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Especialidad> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Especialidad listarPorId(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
