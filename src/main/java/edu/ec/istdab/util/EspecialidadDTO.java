package edu.ec.istdab.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class EspecialidadDTO {
	private List<String> especialidades;
	
	public EspecialidadDTO() {
		String[] arreglo = { "Cardiologia","Ginecología", "Pediatria", "Neumologia", "Dermatología","Oftalmología"};
		this.especialidades = new ArrayList<>(Arrays.asList(arreglo));
	}

	public List<String> getEspecialidades() {
		return especialidades;
	}
	
}
