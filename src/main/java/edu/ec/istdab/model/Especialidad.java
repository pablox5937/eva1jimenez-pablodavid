package edu.ec.istdab.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Especialidad")
public class Especialidad implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idespecialidad;
	
	@Column(name = "nombre_especialidad", nullable = false, length = 50)
	private String nombre_especialidad;

	public Integer getIdespecialidad() {
		return idespecialidad;
	}

	public void setIdespecialidad(Integer idespecialidad) {
		this.idespecialidad = idespecialidad;
	}

	public String getNombre_especialidad() {
		return nombre_especialidad;
	}

	public void setNombre_especialidad(String nombre_especialidad) {
		this.nombre_especialidad = nombre_especialidad;
	}
	
	
}
