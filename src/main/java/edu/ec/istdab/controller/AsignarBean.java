package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Rol;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IPersonaService;
import edu.ec.istdab.service.IRolService;

@Named
@ViewScoped
public class AsignarBean implements Serializable{
	
	private List<Persona> personas;
	private Persona persona;
	private DualListModel<Rol> dual;
	
	@Inject
	private IPersonaService servicePersona;
	
	@Inject
	private IRolService serviceRol;
	
	@PostConstruct
	public void init() {
		this.listarPersonas();
		this.listarRoles();
	}
	
	public void listarPersonas() {
		try {
			this.personas = this.servicePersona.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listarRoles() {
		try {
			List<Rol> roles = this.serviceRol.listar();
			this.dual = new DualListModel<Rol>(roles, new ArrayList<Rol>());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void asignar() {
		try {
			Usuario us = new Usuario();
			us.setId(this.persona.getIdPersona());
			us.setPersona(this.persona);
			serviceRol.asignar(us, this.dual.getTarget());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public DualListModel<Rol> getDual() {
		return dual;
	}

	public void setDual(DualListModel<Rol> dual) {
		this.dual = dual;
	}

	public List<Persona> getPersonas() {
		return personas;
	}
	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	
}
