package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;
import org.primefaces.event.FileUploadEvent;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IPersonaService;
import edu.ec.istdab.service.IUsuarioService;
@Named
@ViewScoped
public class UsuarioBean implements Serializable{
	@Inject
	private IUsuarioService service;
	private Usuario us;
	private String tipoDialogo;
	
	@Inject
	private IPersonaService servicePersona;
	private Persona persona;
	private List<Persona> lista;
	
	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public List<Persona> getLista() {
		return lista;
	}

	public void setLista(List<Persona> lista) {
		this.lista = lista;
	}

	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

	@PostConstruct
	public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		this.us = (Usuario) context.getExternalContext().getSessionMap().get("usuario");
		this.listar();	
	}
	
	public void listar() {
		try {
			Integer id = this.us.getPersona().getIdPersona();
			Persona per = new Persona();
			per.setIdPersona(id);
			this.persona = this.servicePersona.listarPorId(per);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void operar(String accion) {
		try {
			if (accion.equalsIgnoreCase("M")) {
				
				this.service.modificar(us);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario Modificado",""));
			} 
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void operarP(String accion) {
		try {
			
			if (accion.equalsIgnoreCase("M")) {
				this.servicePersona.modificar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Persona Modificada",""));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void mostrarData(Usuario u) {
		this.us = u;
		this.tipoDialogo = "Modificar Usuario";
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.persona.setFoto(event.getFile().getContents());
			}
		} catch (Exception e) {

		}
	}
	
}
