package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;

import edu.ec.istdab.dao.IPersonaDAO;
import edu.ec.istdab.dao.impl.PersonaDAOImpl;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.service.IPersonaService;

@Named
public class PersonaServiceImpl implements IPersonaService, Serializable{
	//para crear la instancia a la interface DAO
	//@Inject
	@EJB
	private IPersonaDAO dao;
	
	/*public PersonaServiceImpl() {
		//dao = new PersonaDAOImpl();
	}*/
	@Override
	public Integer registrar(Persona per) throws Exception {
		
		return dao.registrar(per);
	}

	@Override
	public Integer modificar(Persona per) throws Exception {
		
		return dao.modificar(per);
	}

	@Override
	public Integer eliminar(Persona per) throws Exception {
		
		return dao.eliminar(per);
	}

	@Override
	public List<Persona> listar() throws Exception {
		
		return dao.listar();
	}

	@Override
	public Persona listarPorId(Persona per) throws Exception {
		
		return dao.listarPorId(per);
	}
	
}
