package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import edu.ec.istdab.dao.IEspecialidadDAO;
import edu.ec.istdab.dao.IPersonaDAO;
import edu.ec.istdab.model.Especialidad;
import edu.ec.istdab.service.IEspecialidadService;

@Named
public class EspecialidadServiceImpl implements IEspecialidadService, Serializable{

	@EJB
	private IEspecialidadDAO dao;
	
	@Override
	public Integer registrar(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return  dao.registrar(t);
	}

	@Override
	public Integer modificar(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Especialidad> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Especialidad listarPorId(Especialidad t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
